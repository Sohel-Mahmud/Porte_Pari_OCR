package battlemage.portepariocr;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Arrays;

public class ScanViewInfo extends AppCompatActivity {

    TextView txtName, txtRoll, txtSec;
    String name, roll, sec, resultTest,result;
    String chk="name";
    String chk1="roll";
    String chk2="sec";
    int[] count = new int[4];
    int nameLoc = 0, rollLoc=0, secLoc=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_view_info);

        txtName = (TextView)findViewById(R.id.txtName);
        txtRoll = (TextView)findViewById(R.id.txtRoll);
        txtSec = (TextView)findViewById(R.id.txtSec);

        resultTest = getIntent().getExtras().getString("Scan");

        //replace all new line with space
        result = resultTest.replaceAll("\\r\\n|\\r|\\n", " ");

        //split each word
        String []p=result.replaceAll("[^a-zA-Z919293949596979899100 ]", "").toLowerCase().split("\\s+");
        count[3]=p.length;
        for(int i=0; i<p.length; i++){
            System.out.print(p[i]);
        }

        for(int i=0; i<p.length; i++){
            if(chk.equals(p[i])){
                nameLoc = i;
                count[0]=i;
                System.out.println("founsd");
            }
            else if(chk1.equals(p[i])){
                rollLoc = i;
                count[1]=i;
                System.out.println("roll found");
            }
            else if(chk2.equals(p[i])){
                secLoc = i;
                count[2]=i;
                System.out.println("sec found");
            }
        }
        Arrays.sort(count);
        for (int i=0; i<count.length;i++) {

            System.out.println(i+" "+count[i]);
        }
        String s1[] = check(count[0],count[1],p);
        //System.out.println(s1);
        /*for(int i=0; i<s1.length; i++){
            System.out.print(s1[i]);
        }*/
        checkCatagory(s1);
        String s2[] = check(count[1],count[2],p);
        //System.out.println(s2);
        checkCatagory(s2);

        String s3[] = check(count[2],count[3],p);
        //System.out.println(s3);
        checkCatagory(s3);
    }


    private String[] check(int n1, int n2, String[] pc) {

        String[] checked = new String[n2-n1];

        for(int i=n1,j=0; i<n2; i++,j++){
            if(n1==nameLoc){
                checked[j] = pc[i]+" ";

            }
            else if(n1==rollLoc){
                checked[j] = pc[i]+" ";

            }
            else if(n1==secLoc){
                checked[j] = pc[i]+" ";

            }

        }

        // String newString =Arrays.toString(checked).replaceAll(", |\\[|\\]", "").trim();
        //newString = newString.substring(1, newString.length()-1);
        //return newString;
        return checked;
    }
    private void checkCatagory(String[] s1) {

        int nameFlag=0,rollFlag=0,secFlag=0;
        String chk="name ";
        String chk1="roll ";
        String chk2="sec ";

        for(int i=1; i<s1.length; i++){

            if(chk.equals(s1[0])){
                nameFlag=1;
                break;
            }
            else if(chk1.equals(s1[0])){
                rollFlag=1;
                break;
            }
            else if(chk2.equals(s1[0])){
                secFlag=1;
                break;
            }

        }
        if(nameFlag==1){
            String newString1 =Arrays.toString(s1).replaceAll(", |\\[|\\]", "").trim();
            System.out.println(newString1+" is the name");
            txtName.setText(newString1);

        }
        else if(rollFlag==1){
            String newString2 =Arrays.toString(s1).replaceAll(", |\\[|\\]", "").trim();
            System.out.println(newString2+" is the roll");
            txtRoll.setText(newString2);
        }

        else if(secFlag==1){
            String newString3 =Arrays.toString(s1).replaceAll(", |\\[|\\]", "").trim();
            System.out.println(newString3+" is the section");
            txtSec.setText(newString3);
        }

    }
    }

