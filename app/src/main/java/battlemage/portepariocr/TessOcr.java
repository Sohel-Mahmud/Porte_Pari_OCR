package battlemage.portepariocr;

import android.graphics.Bitmap;

import com.googlecode.tesseract.android.TessBaseAPI;

/**
 * Created by battlemage on 10/26/2017.
 */

public class TessOcr {
    private TessBaseAPI mTess;

    public TessOcr(){
        mTess = new TessBaseAPI();
        String datapath = FileManagement.instance.getTessDataPathDirectory();
        mTess.init(datapath,"eng");
    }
    public String getOCRResult(Bitmap bitmap) {

        mTess.setImage(bitmap);
        String result = mTess.getUTF8Text();

        return result;
    }

    public void onDestroy() {
        if (mTess != null)
            mTess.end();
    }

}
